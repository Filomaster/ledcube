/*
 Name:		LEDcube.cpp
 Created:	13.05.2019 11:35:08
 Author:	Filomaster
 Editor:	http://www.visualmicro.com
*/

#include "LEDcube.h"
#include "Arduino.h"

#define default_DS 10
#define default_STCP 9
#define default_SHCP 8

//Binary values of each digital pins of arduino
// Digital Pins 0-7 with D index in commands
#define PORT_0 B00000001
#define PORT_1 B00000010
#define PORT_2 B00000100
#define PORT_3 B00001000
#define PORT_4 B00010000
#define PORT_5 B00100000
#define PORT_6 B01000000
#define PORT_7 B10000000
//Digital Pins 8-13 with B index in commands
#define PORT_8 B00000001
#define PORT_9 B00000010
#define PORT_10 B00000100
#define PORT_11 B00001000
#define PORT_12 B00010000
#define PORT_13 B00100000

LEDcube::LEDcube(int cube_size)
{
	DS_PIN = default_DS;
	STCP_PIN = default_STCP;
	SHCP_PIN = default_SHCP;
	SetOutput(DS_PIN);
	SetOutput(STCP_PIN);
	SetOutput(SHCP_PIN);
	
	SIZE = cube_size;
}

LEDcube::LEDcube(int cube_size, int DS_pin, int STCP_pin, int SHCP_pin)
{
	DS_PIN = DS_pin;
	STCP_PIN = STCP_pin;
	SHCP_PIN = SHCP_pin;
	SetOutput(DS_PIN);
	SetOutput(STCP_PIN);
	SetOutput(SHCP_PIN);
	
}

//This method adds 
void LEDcube::SetOutput(int Pin) {

	
	switch (Pin)
	{
	case 0:
		BINARY_OUTPUT_D += PORT_0;
		break;
	case 1:
		BINARY_OUTPUT_D += PORT_1;
		break;
	case 2:
		BINARY_OUTPUT_D += PORT_2;
		break;
	case 3:
		BINARY_OUTPUT_D += PORT_3;
		break;
	case 4:
		BINARY_OUTPUT_D += PORT_4;
		break;
	case 5:
		BINARY_OUTPUT_D += PORT_5;
		break;
	case 6:
		BINARY_OUTPUT_D += PORT_6;
		break;
	case 7:
		BINARY_OUTPUT_D += PORT_7;
		break;
	case 8:
		BINARY_OUTPUT_B += PORT_8;
		break;
	case 9:
		BINARY_OUTPUT_B += PORT_9;
		break;
	case 10:
		BINARY_OUTPUT_B += PORT_10;
		break;
	case 11:
		BINARY_OUTPUT_B += PORT_11;
		break;
	case 12:
		BINARY_OUTPUT_B += PORT_12;
		break;
	case 13:
		BINARY_OUTPUT_B += PORT_13;
		break;
	}
}

void LEDcube::BinaryClear() {
	BINARY_OUTPUT_B = B00000000;
	BINARY_OUTPUT_D = B00000000;
}

void LEDcube::Setup() 
{
	//Setting chosen pins as output
	DDRD = BINARY_OUTPUT_D;
	DDRB = BINARY_OUTPUT_B;
	//Setting number of swift reisters
	REGISTER_COUNT = ((SIZE * (SIZE + 1)) / 8 + 0.99);
	BinaryClear();
	
}

void LEDcube::Clear() 
{

}

void LEDcube::SetDisplay() {
	PORTD = SHCP_PIN; 
	//ten pin jest z�y, poniewa� powinien by� na LOW... Poza tym musz� wzi�� od uwag� mo�liwo��
	// wyst�pienia tak�e u�ycia pin�w 8-13 (czyli PORTB)... Musz� si� powa�nie zastanowi�nad drog� jak� osi�gn� ten cel
	
}


