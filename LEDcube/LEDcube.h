/*
 Name:		LEDcube.h
 Created:	13.05.2019 11:35:08
 Author:	Filomaster
 Editor:	http://www.visualmicro.com
*/

#ifndef LEDcube_h
#define LEDcube_h

#include "Arduino.h"

class LEDcube
{
	public:
		
		//Think abaut super-speed
	 	//Constructor with default register pins:
		LEDcube(int cube_size);
		//Constructor with custom register pins:
		LEDcube(int cube_size, int DS_pin, int STCP_pin, int SHCP_pin);
		//Public methods:
		void Clear();
		void SetDisplay();


	private:
		int DS_PIN, STCP_PIN, SHCP_PIN;
		int SIZE;
		int REGISTER_COUNT;
		int BINARY_OUTPUT_D = B0000000;
		int BINARY_OUTPUT_B = B00000;
		void BinaryClear();
		void Setup(); //Setup with default pin setting
		void SetOutput(int Pin);
};


#endif

